
const dummya = ['Fermentum porttitor quisque lacinia. Proin venenatis hendrerit rutrum rutrum facilisis imperdiet adipiscing consequat accumsan natoque. Curabitur habitant ultricies suspendisse commodo pretium elit. Pulvinar. Ante integer condimentum aptent. Platea senectus habitasse. Porttitor tortor hendrerit imperdiet. Porta mi suscipit platea. Hymenaeos euismod viverra maecenas penatibus tincidunt.',
'Taciti, class curae; accumsan amet sem lobortis tempor fringilla arcu hendrerit varius id mi pellentesque habitant quis ridiculus, congue penatibus.',
'Tempus, hendrerit nisi conubia ut dictumst torquent. Tempus lobortis nisl nam iaculis pellentesque varius aliquam ac mattis euismod. Blandit congue eu justo fusce. Pulvinar mus leo est libero iaculis class. Justo mi interdum bibendum montes blandit commodo conubia pellentesque adipiscing.',
'Eget porttitor laoreet tincidunt, etiam. Vitae senectus tincidunt. Viverra bibendum mus blandit mus porta primis sollicitudin sapien praesent sed. Et netus dolor. Habitant dui, vel volutpat per mi posuere ipsum platea integer dis.',
'Morbi tristique. Curae; ut vulputate imperdiet neque phasellus commodo metus maecenas nostra a. Malesuada fusce tempus. Cubilia tortor amet.',

'Ultrices sodales netus, placerat vehicula ultricies ultrices elementum dis litora. Facilisi morbi praesent massa mus, justo rhoncus class ac elit. Taciti class pharetra dignissim, faucibus mus sollicitudin eu turpis phasellus enim. Lacinia sociis. Semper imperdiet pretium suscipit porta. Tortor.',
'Curae; aliquet a purus purus. Mattis diam volutpat suspendisse ac massa. Neque. Semper aliquam cum cubilia. Eleifend congue eleifend ultrices iaculis vel. Vehicula hendrerit vel fringilla ad fermentum phasellus platea leo felis erat nibh euismod magna adipiscing arcu. Aliquet libero, morbi quisque dis, eleifend.',
'Amet eget lorem arcu Ridiculus tellus nam molestie risus penatibus in curae; rhoncus sapien vestibulum nam, elementum morbi ornare vehicula placerat vel dolor pharetra maecenas vehicula venenatis interdum. Porta. Semper molestie nonummy fringilla consectetuer tempor lectus aliquet cras tristique luctus orci tempus facilisis.','Facilisis volutpat ridiculus taciti. Ultricies scelerisque senectus nostra ante quisque nec, aptent varius consectetuer molestie conubia convallis sollicitudin.']

// Catch All
const form = document.querySelector('#form')
const inp = document.querySelector('#inp')
const btn = document.querySelector('#btn')
const show = document.querySelector('#show')


form.addEventListener('submit', (e) => {
e.preventDefault()

const value = parseInt(inp.value)
const random = Math.floor(Math.random() * dummya.length)

if (isNaN(value) || value <= 0 || value > 9 ) {
  show.innerHTML= `<p> ${dummya[random]} </p>`
}

else {
  const sarr = dummya.slice(0,value)
  const mapped = sarr.map((e) => {
    return `<p> ${e} </p>`
  }).join("")
  show.innerHTML=`<p> ${mapped} </p>`
}

})



 
